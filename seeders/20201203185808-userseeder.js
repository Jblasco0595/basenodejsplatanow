'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('users', [
      {
        name: "anniel",
        lastname: "luna",
        telefonnumber: "2211231232",
        email: "anniel@gmail.com",
        review: "10",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Gabriel",
        lastname: "Guerrero",
        telefonnumber: "2219631254",
        email: "gabriel@gmail.com",
        review: "9",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Jolfran",
        lastname: "Palacios",
        telefonnumber: "2217148256",
        email: "jolfran@gmail.com",
        review: "5",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "Luis",
        lastname: "De La Cruz",
        telefonnumber: "2216520189",
        email: "luis@gmail.com",
        review: "10",
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        name: "jorge",
        lastname: "blasco",
        telefonnumber: "2217953125",
        email: "jorge@gmail.com",
        review: "4",
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
