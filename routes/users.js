'use strict'
 
// Cargamos el módulo de express para poder crear rutas
const express = require('express');
 
// Llamamos al router
const router = express.Router();
 
router.get('/', (req, res) => res.send(
    'bienvenido, te encuentras en el area de usuarios solo puedes enlistar los usuarios actuales',
));

router.get('/list', (req, res) => res.send(
    'enlista a los usuarios!',
));

module.exports = router;
