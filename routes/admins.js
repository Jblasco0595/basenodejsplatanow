'use strict'
const usersdb = require ('../models/index');
// Cargamos el módulo de express para poder crear rutas
const express = require('express');

//instanciamos router
const adminRouter = express.Router();

//página principal del admin, panel de administración/dashboard 
adminRouter.get('/', function(req, res) {
    res.send('<h1>¡Bienvenido, te encuentras en el panel de administrados podras enlistar, crear y eliminar usuarios!</h1>');
});

adminRouter.get('/adminG', function(req, res) {
    res.send('<h1>¡Bienvenido, Admin Go!</h1>');
});
 
//list user page 
adminRouter.get('/list', ( req, res, next ) => {
    usersdb.user.findAll()
    .then( userResponse => {
        res.status( 200 ).json( userResponse )
      })
      .catch( error => {
        res.status( 400 ).send( error )
      })
});
 
//posts page 
adminRouter.post('/create', (req, res, next) => {
    let { name, lastname, telefonnumber, email, review } = req.body
    usersdb.user.create( { name, lastname, telefonnumber, email, review })
        .then(user => res.json(user))
        .catch( error => {
            res.status( 400 ).send( error )
        });
})

//put page 
adminRouter.put('/update/:userId', (req, res, next) => { 
    let { name, lastname, telefonnumber, email, review } = req.body
    usersdb.user.update({ name, lastname, telefonnumber, email, review }, {
        where: {id: req.params.userId}
    });
    res.json({ success: 'modificado'})
});

//posts page 
adminRouter.delete('/delete/:userId', (req, res, next) => {
    usersdb.user.destroy({ 
        where: { id: req.params.userId }
    });
    res.json({ success: 'se ha eliminado al usuario'})
});

module.exports = adminRouter;