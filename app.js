const express       = require('express');
const logger        = require('morgan');
const bodyParser    = require('body-parser');
const path          = require('path');
const mysql = require('mysql');
const http = require('http');
const app = express();
require('dotenv').config();

//importando las rutas 
const usersRouter = require('./routes/users');
const adminsRouter = require('./routes/admins');

// This will be our application entry. We'll setup our server here.

// Set up the express app

// Log requests to the console.
app.use(logger('dev'));
// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// Setup a default catch-all route that sends back a welcome message in JSON format.

//middlewares

// routes
app.use('/', usersRouter);
app.use('/admin', adminsRouter);

const port = parseInt(process.env.PORT, 10) || 8000;
app.set('port', port);
const server = http.createServer(app);
server.listen(port);
module.exports = app;